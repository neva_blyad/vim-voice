# NAME

Vim Voice

# VERSION

0.09

# DESCRIPTION

Vim editor voice control

# FEATURES

TODO

# HIGHLIGHTS AND BACKGROUND

This application uses Vosk offline speech recognition.
Grammar is taught and targeted for working with the Russian Vosk model.

(In future, the support of another speech engines, such as Mozilla's DeepSpeech,
possibly, would be added.)

# SCREENCAST

TODO

# INSTALL

INSTALL file describes how to install the vim-voice.

# USAGE

Start vosk-server.
There are 4 servers. We use the WebSocket one, written in C++.

$ cd vosk-server/websocket-cpp/
$ ./asr-server 0.0.0.0 8080 1 ../../vosk-model-small-ru-*/

If you hadn't reboot your system after the Vim Voice installation, start it
manually. Otherwise, it should be already started.

$ systemctl --user start vim-voiced.service

# FAQ

TODO

# FOR HACKERS

See TODO.

# IN LOVING MEMORY OF

Irina
Natalia
Daria
Daria
