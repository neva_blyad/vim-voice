#!/usr/bin/env raku

=begin pod

=head1 NAME

vim-voice
vim-voiced.raku

=head1 VERSION

0.09

=head1 DESCRIPTION

Vim editor voice control

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=end pod

# Features
use experimental :pack;

# Specify path to libraries
use lib 'lib/';

# Raku modules
use Audio::PortAudio;
use Cro::Transform;
use Cro::WebSocket::Client;
use JSON::Fast;
use LogP6 :configure; # Use library in configure mode
use NativeCall;
use NativeHelpers::Blob;

# Our own modules
use grammar;

# General my constants
my constant EOF = '{"eof" : 1}' ~ "\0";
my constant LOG-CFG-PATH = %*ENV<HOME>.IO.add('.config/vim-voiced_log-p6.json');

# Audio settings
my constant NUM-CHANNELS    =       1;
my constant SAMPLE-RATE-HZ  =   44100;
my constant BITS-PER-SAMPLE =      16;
my constant FILE-SIZE       = 4000000;

my constant BYTE-RATE       = Int(SAMPLE-RATE-HZ * BITS-PER-SAMPLE * NUM-CHANNELS / 8);
my constant BLOCK-ALIGN     = Int(BITS-PER-SAMPLE * NUM-CHANNELS / 8);

# Network settings
my constant HOST = 'localhost';
my constant PORT = 8080;

my constant CONN-DELAY-BEFORE-RE-S = 10;
my constant CLOSE-TOUT-S = 2;

# Vim settings
my constant VIM-CMD  = 'vim';
my constant SRV-NAME = 'lato';

# Initialize logger
my sub logger_init(
    Bool :$log-stdout,   # Logging to STDOUT
    Bool :$log-file,     # Logging to file
    Bool :$log-journald, # Logging to journald
    Bool :$verbose,      # Display verbose output
    Bool :$libre         # Do logging liberation
)
{
    # Setup logger
    #  - configuration file,
    my $cfg-path;

    if 'log-p6.json'.IO.e
    {
        $cfg-path = 'log-p6.json';
    }
    else
    {
        chdir %*ENV<HOME>;
        $cfg-path = LOG-CFG-PATH;
    }

    init-from-file($cfg-path);

    #  - pattern,
    my Str $date;
    my Str $lvl;
    my Str $threads;
    my Str $msg;

    if $libre
    {
        $date    = '%color{TRACE=38;5;56 DEBUG=38;5;56 INFO=38;5;56 WARN=38;5;56 ERROR=38;5;56}[%date{$hh:$mm:$ss:$mss}]%creset';
        $lvl     = ' %color{TRACE=38;5;154 DEBUG=38;5;154 INFO=38;5;163 WARN=1;38;5;199 ERROR=1;38;5;196}[%level]%creset';
        $threads = ' %color{TRACE=38;5;22 DEBUG=38;5;22 INFO=38;5;22 WARN=38;5;22 ERROR=38;5;22}[%tname:%tid]%creset' if $verbose;
        $msg     = ' %msg';
    }
    else
    {
        $date    = '%color{TRACE=blue DEBUG=blue INFO=blue WARN=blue ERROR=blue}[%date{$hh:$mm:$ss:$mss}]%creset';
        $lvl     = ' %color{TRACE=yellow DEBUG=yellow INFO=blue WARN=magenta ERROR=red}[%level]%creset';
        $threads = ' %color{TRACE=green DEBUG=green INFO=green WARN=green ERROR=green}[%tname:%tid]%creset' if $verbose;
        $msg     = ' %msg';
    }

    set-default-pattern("$date$lvl$threads$msg");

    #  - filter,
    filter(:name('filter-verbosity'),
           :level($verbose ?? $trace !! $info));

    #  - grooves,
    my @grooves = ();

    if $log-stdout   { push @grooves, 'writer-stdout',   'filter-verbosity'; }
    if $log-file     { push @grooves, 'writer-file',     'filter-verbosity'; }
    if $log-journald { push @grooves, 'writer-journald', 'filter-verbosity'; }

    #  - cliche
    cliche(:name('cliche-out'),
           :matcher('cliche-out'),
           :grooves(@grooves));

    return get-logger('cliche-out');
}

# Program entry point.
#
# We send recorded stream from a microphone to vosk-server WebSocket server.
# The parsed text is to be received back.
# We parse it using Raku grammars, get Vim command, send them to Vim server.
subset size of Int where * > 0;

my sub MAIN(
    size :$buf_size     = 4096,  #= Buffer size for input audio stream

    Bool :$log-stdout   = False, #= Logging to STDOUT
    Bool :$log-file     = False, #= Logging to file
    Bool :$log-journald = False, #= Logging to journald
    Bool :v($verbose)   = False, #= Display verbose output
    Bool :$libre        = False, #= Do logging liberation
)
{
    # Initialize logger
    my $log = logger_init(:$log-stdout,
                          :$log-file,
                          :$log-journald,
                          :$verbose,
                          :$libre);

    $log.debug('Application: start');

    # Concurrency variables:
    #  - declaration
    my Promise $promise-stream-read;
    my Promise $promise-conn;
    my Promise $promise-conn-read;
    my Promise $promise-conn-write;

    my Promise $_promise-stream-read;
    my Promise $_promise-conn;
    my Promise $_promise-conn-read;
    my Promise $_promise-conn-write;

    my Sub $sub-stream-read;
    my Sub $sub-stream-read-then;
    my Sub $sub-conn;
    my Sub $sub-conn-then;
    my Sub $sub-conn-read;
    my Sub $sub-conn-read-then;
    my Sub $sub-conn-write;
    my Sub $sub-conn-write-then;

    my Lock    $lock-conn;
    my Channel $channel-conn-write;

    my Cro::WebSocket::Client::Connection $conn;

    #  - initialization
    $_promise-stream-read = Promise.new;
    $_promise-conn        = Promise.new;
    $_promise-conn-read   = Promise.new;
    $_promise-conn-write  = Promise.new;

    $lock-conn          = Lock.new;
    $channel-conn-write = Channel.new;

    # Handle SIGINT
    signal(SIGINT).act(
    {
        $log.debug('Handled SIGINT');

        $_promise-stream-read.keep;
        $_promise-conn.keep;
        $_promise-conn-read.keep;
        $_promise-conn-write.keep;
    });

    ################################################################
    # Stream Read Promise
    ################################################################

    # Read from the input audio stream
    my Audio::PortAudio         $port-audio;
    my Audio::PortAudio::Stream $in-stream;

    $sub-stream-read = sub
    {
        $log.debug('Stream Read Promise: start');

        # Access to audio input devices
        $port-audio = Audio::PortAudio.new;
        $in-stream  = $port-audio.open-default-stream(NUM-CHANNELS,
                                                      0, # No output channels
                                                      Audio::PortAudio::StreamFormat::Int16,
                                                      SAMPLE-RATE-HZ,
                                                      $buf_size);

        # Start recording from the microphone
        $in-stream.start;

        # Read from the input audio stream
        loop
        {
            # Read stream
            last if $_promise-stream-read.status ~~ Kept; # TODO: SIGINT doesn't unblock read() function, fix

            my CArray[int16] $arr  = $in-stream.read($buf_size, NUM-CHANNELS, int16);
            my Pointer       $ptr  = nativecast(Pointer, $arr);
            my size_t        $size = 2 * $arr.elems;
            my Blob          $buf  = blob-from-pointer($ptr, :elems($size), :type(Blob[uint8]));

            # Send to WebSocket
            $channel-conn-write.send($buf);
        }

        # Stop recording from the microphone
        $in-stream.close;
        $port-audio.terminate;
    }

    $sub-stream-read-then = sub
    {
        $log.debug('Stream Read Promise: stop');
    }

    ################################################################
    # Connection Promise
    ################################################################

    # Connect to the vosk-server via WebSocket
    $sub-conn = sub
    {
        $log.debug('Connection Promise: start');

        # Close the connection
        if $conn
        {
            $_promise-conn-read.keep;
            $_promise-conn-write.keep;

            await $promise-conn-read;
            await $promise-conn-write;

            $_promise-conn-read  = Promise.new;
            $_promise-conn-write = Promise.new;

            react
            {
                whenever $conn.close(timeout => CLOSE-TOUT-S) { done; }
                whenever $_promise-conn { done; }
            }
        }

        # Connect to the vosk-server via WebSocket
        if $_promise-conn.status ~~ Planned
        {
            loop
            {
                $log.info('Connecting to ' ~ HOST ~ ':' ~ PORT ~ '...');

                # Try to connect
                try
                {
                    state $client = Cro::WebSocket::Client.new:
                        uri => 'ws://' ~ HOST ~ ':' ~ PORT ~ '/';

                    react
                    {
                        whenever $client.connect { $conn = $_; done; }
                        whenever $_promise-conn { done; }
                    }

                    CATCH
                    {
                        default
                        {
                            $log.warn(tclc .Str);
                            sleep CONN-DELAY-BEFORE-RE-S;
                            next;
                        }
                    }
                }

                # Now start Connection Read & Write Promises
                if $_promise-conn.status ~~ Planned
                {
                    $log.info('Connection success');

                    $promise-conn-read  = Promise.start: $sub-conn-read;
                    $promise-conn-write = Promise.start: $sub-conn-write;

                    $promise-conn-read.then:  $sub-conn-read-then;
                    $promise-conn-write.then: $sub-conn-write-then;
                }

                last;
            }
        }
    }

    $sub-conn-then = sub
    {
        $log.debug('Connection Promise: stop');
    }

    ################################################################
    # Connection Read Promise
    ################################################################

    # Read from the WebSocket connection
    $sub-conn-read = sub
    {
        $log.debug('Connection Read Promise: start');

        my constant ERR = 'Connection closed by remote host';

        react
        {
            whenever $conn.messages() -> $msg
            {
                whenever $msg.body() -> $body
                {
                    # Output the body to the STDOUT.
                    # Apply the grammar if Insert mode is active or send it as is
                    # otherwise.
                    # Output parsed command.
                    # Send command or text to the Vim server (using UNIX socket).
                    my $json = JSON::Fast::from-json($body);
                    next unless $json.isa(Hash);

                    if $json<text>:exists
                    {
                        # Output the body to the STDOUT
                        my $inp = $json<text>;
                        next if $inp.chars == 0;
                        
                        $log.debug('Input: ' ~ $inp);

                        # Get Vim mode
                        my Proc $proc = run VIM-CMD, '--servername',  SRV-NAME,
                                                     '--remote-expr', 'mode()', :out;
                        my Str  $mode = $proc.out.slurp(:close).trim;

                        $log.debug('Vim mode: ' ~ $mode);

                        if $mode eq 'i'
                        {
                            # Vim is running in Insert mode.
                            #
                            # Try to find <ESC>, <Enter> commands in voice sample.
                            my Match $parsed;

                            $parsed = VimCmd.parse($inp, :actions(VimCmdAction), :rule<cmd:sym<escape>>);
                            $parsed = VimCmd.parse($inp, :actions(VimCmdAction), :rule<cmd:sym<enter>>) unless $parsed;

                            if $parsed
                            {
                                # Some command was found.
                                # We are going to execute it.
                                #
                                # Send the command to the Vim server (using UNIX socket) without
                                # parsing it with grammar.
                                my Str @cmds = $parsed.made;

                                if @cmds.elems == 1
                                {
                                    run VIM-CMD, '--servername',  SRV-NAME,
                                                 '--remote-send', @cmds[0];
                                }
                            }
                            else
                            {
                                # No command was found.
                                #
                                # Send all the JSON body to Vim server (using UNIX socket)
                                # without parsing it with grammar.
                                run VIM-CMD, '--servername',  SRV-NAME,
                                             '--remote-send', $inp ~ ' ';
                            }
                        }
                        else
                        {
                            # Vim is running in another mode. (Normal is most possibly.)
                            #
                            # Apply the grammar.
                            my Match $parsed = VimCmd.parse($inp, :actions(VimCmdAction));
                            next unless $parsed;

                            # Output parsed commands.
                            # Send the command to the Vim server.
                            my Str @cmds = $parsed.made;

                            for @cmds -> Str $cmd
                            {
                                $log.debug('Parsed:   ' ~ $cmd);

                                #my Str $vim-cmd = ':' ~ $cmd ~ '<CR>';
                                my Str $vim-cmd = $cmd;

                                run VIM-CMD, '--servername',  SRV-NAME,
                                             '--remote-send', $vim-cmd;
                            }
                        }
                    }
                }

                LAST { $log.warn(ERR); done; }
                QUIT { $log.warn(ERR); done; }

                whenever $_promise-conn-read { done; }
            }

            LAST { $log.warn(ERR); done; }
            QUIT { $log.warn(ERR); done; }

            whenever $_promise-conn-read { done; }
        }

        if $_promise-conn-read.status ~~ Planned
        {
            $lock-conn.protect(
            {
                if $promise-conn.status ~~ Kept
                {
                    $promise-conn = Promise.start: $sub-conn;
                    $promise-conn.then: $sub-conn-then;
                }
            });
        }
    }

    $sub-conn-read-then = sub
    {
        $log.debug('Connection Read Promise: stop');
    }

    ################################################################
    # Connection Write Promise
    ################################################################

    # Write to the WebSocket connection
    $sub-conn-write = sub
    {
        $log.debug('Connection Write Promise: start');

        state Blob $now;
        state Blob $later;

        my Int $total = 0;

        enum Res <CONTINUE RECONN ABORT>;
        my Res $res;

        # Split the outcoming buffer into two
        my sub split(Blob $buf,
                     Int  $total is rw,

                     --> List)
        {
            my Int  $size  = $total + $buf.elems > FILE-SIZE ?? FILE-SIZE - $total !!
                                                                $buf.elems;
            my Blob $now   = $buf.subbuf(0, $size);
            my Blob $later = $buf.subbuf($size);

            return ($now, $later);
        }

        # Write to the WebSocket connection
        my sub send(Blob $buf,
                    Int  $total is rw,

                    --> Res)
        {
            my $res;

            react
            {
                # Write to the WebSocket connection
                whenever $conn.send($buf)
                {
                    $total += $buf.elems;
                    $res    = $total == FILE-SIZE ?? RECONN !! CONTINUE;

                    done;
                }

                whenever $_promise-conn-write
                {
                    $res = ABORT;
                    done;
                }
            }

            return $res;
        }

        # Send WAV file
        try
        {
            # WAV file header
            state Blob $wav-hdr = pack '<a4La4a4LSSLLSSa4L',
                'RIFF',
                FILE-SIZE - 8, # Chunk size
                'WAVE',
                'fmt ',
                16,
                0x0001, # PCM
                NUM-CHANNELS,
                SAMPLE-RATE-HZ,
                BYTE-RATE,
                BLOCK-ALIGN,
                BITS-PER-SAMPLE,
                'data',
                FILE-SIZE - 44; #chunk2 size

            # Send the WAV file header
            $res = send($wav-hdr, $total);

            # Send the WAV body
            while $res ~~ CONTINUE
            {
                if $now
                {
                    # Write to the WebSocket connection
                    $res = send($now, $total);

                    # Split the outcoming buffer into two
                    ($now, $later) = split($later, $total) unless $res ~~ ABORT;
                }
                else
                {
                    react
                    {
                        whenever $channel-conn-write -> Blob $buf
                        {
                            # Split the outcoming buffer into two
                            ($now, $later) = split($buf, $total);

                            # Write to the WebSocket connection
                            $res = send($now, $total);

                            # Split the outcoming buffer into two
                            ($now, $later) = split($later, $total) unless $res ~~ ABORT;

                            done;
                        }
                        LAST { $res = RECONN; done; }
                        QUIT { $res = RECONN; done; }

                        whenever $_promise-conn-write
                        {
                            $res = ABORT;
                            done;
                        }
                    }
                }
            }

            CATCH
            {
                default
                {
                    $res = RECONN;
                    $log.warn(tclc .Str);
                }
            }
        }

        if $res ~~ RECONN
        {
            $lock-conn.protect(
            {
                if $promise-conn.status ~~ Kept
                {
                    $promise-conn = Promise.start: $sub-conn;
                    $promise-conn.then: $sub-conn-then;
                }
            });
        }
    }

    $sub-conn-write-then = sub
    {
        $log.debug('Connection Write Promise: stop');
    }

    ################################################################
    # Start Promises
    ################################################################

    $promise-stream-read = Promise.start: $sub-stream-read;
    $promise-stream-read.then: $sub-stream-read-then;

    $promise-conn = Promise.start: $sub-conn;
    $promise-conn.then: $sub-conn-then;

    #$promise-conn-read = Promise.start: $sub-conn-read; # We'll start this Promise when connected
    #$promise-conn-read.then: $sub-conn-read-then;

    #$promise-conn-write = Promise.start: $sub-conn-write; # We'll start this Promise when connected
    #$promise-conn-write.then: $sub-conn-write-then;

    ################################################################
    # Clean up
    ################################################################

    await $promise-stream-read;
    await $promise-conn;
    await $promise-conn-read;
    await $promise-conn-write;

    if $conn
    {
        await $conn.close(timeout => CLOSE-TOUT-S); # Close with timeout
        $conn.close(:!timeout);                     # Forceful close, no timeout
    }

    $log.debug('Application: stop');
}
