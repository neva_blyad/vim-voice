=begin pod

=head1 NAME

vim-voice
grammar.rakumod

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

=end pod

# Raku modules
#use Grammar::Tracer; # Turn on grammar debugging

# Vim command
grammar VimCmd
{
    # Single or multiple commands
    rule TOP
    {
        \s*<cmd-with-num>[\s+<cmd-with-num>]*\s*
    }

    # Entire command
    token cmd-with-num
    {
        <num-full>?<cmd>
    }

    # Number part
    rule num-full
    {
        <num>[\s+<num>]*
    }

    proto token num {*}

    token num:sym<num-0>
    {
        'ноль' | 'ноля'  | 'нолю'  | 'нолём'  | 'ноле'  | #`( ед. ч. )
        'ноли' | 'нолей' | 'нолям' | 'нолями' | 'нолях' | #`( мн. ч. )
        'нуль' | 'нуля'  | 'нулю'  | 'нулём'  | 'нуле'  | #`( ед. ч. )
        'нули' | 'нулей' | 'нулям' | 'нулями' | 'нулях'   #`( мн. ч. )
    }

    token num:sym<num-1>
    {
        'один' | 'одного' | 'одному' | 'одним' | 'одном' | #`( м. р. )
        'одно' |                                           #`( c. р. )
        'одна' | 'одной'  | 'одну'   | 'одной' | 'одною' | #`( ж. р. )
        'одни' | 'одних'  | 'одними'                       #`( мн. ч. )
    }

    token num:sym<num-2>
    {
        'два'  | 'двух'  | 'двум'  | 'двумя'  |         #`( м. р., с. р. )
        'две'  |                                        #`( ж. р. )
        'двое' | 'двоих' | 'двоим' | 'двоими' | 'двоих' #`( собирательные )
    }

    token num:sym<num-3>
    {
        'три'  | 'ты'    | 'при'   | 'трёх' | 'трём' | 'тремя' | #`( м. р., с. р., ж. р. )
        'трое' | 'троих' | 'троим' | 'троими'                    #`( собирательные)
    }

    token num:sym<num-4>
    {
        'четыре'  | 'четырёх'  | 'четырём'  | 'четырьмя' | #`( м. р., с. р., ж. р. )
        'четверо' | 'четверых' | 'четверым' | 'четверыми'  #`( собирательные)
    }

    token num:sym<num-5>
    {
        'пять'   | 'пяти'    | 'пятью'   |                        #`( м. р., с. р., ж. р. )
        'пятеро' | 'пятерых' | 'пятерым' | 'пятерых' | 'пятерыми' #`( собирательные)
    }

    token num:sym<num-6>
    {
        'шесть'   | 'шести'    | 'шестью'   | 'шестьми' | #`( м. р., с. р., ж. р. )
        'шестеро' | 'шестерых' | 'шестерым' | 'шестерыми' #`( собирательные)
    }

    token num:sym<num-7>
    {
        'семь'   | 'семи'    | 'семью'   | #`( м. р., с. р., ж. р. )
        'семеро' | 'семерых' | 'семерым' | 'семерыми' #`( собирательные)
    }

    token num:sym<num-8>
    {
        'восемь'   | 'восьми'    |                                          #`( м. р., с. р., ж. р. )
        'восьмеро' | 'восьмерых' | 'восьмерым' | 'восьмерых' | 'восьмерыми' #`( собирательные)
    }

    token num:sym<num-9>
    {
        'девять'   | 'детям'     | 'девяти'    | 'девятьми' | #`( м. р., с. р., ж. р. )
        'девятеро' | 'девятерых' | 'девятырым' | 'девятирыми' #`( собирательные)
    }

    token num:sym<num-10>
    {
        'десять'   | 'десяти'    | 'десятьми'  |              #`( м. р., с. р., ж. р. )
        'десятеро' | 'десятерых' | 'десятерым' | 'десятирыми' #`( собирательные)
    }

    token num:sym<num-11>
    {
        'одиннадцать' | 'одиннадцати' | 'одинадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-12>
    {
        'двенадцать' | 'двенадцати' | 'двенадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-13>
    {
        'тринадцать' | 'трианадцати' | 'транадцатю' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-14>
    {
        'четырнадцать' | 'четырнадцати' | 'четернадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-15>
    {
        'пятнадцать' | 'пятнадцати' | 'пятнадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-16>
    {
        'шестнадцать' | 'шестнадцати' | 'шестнадцаью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-17>
    {
        'семнадцать' | 'семнадцати' | 'семнадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-18>
    {
        'восемнадцать' | 'восемнадцати' | 'восемнадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-19>
    {
        'девятнадцать' | 'девятнадцати' | 'девятнадцатью' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-20>
    {
        'двадцать'   | 'двадцати' | 'двадцатью' | #`( м. р., с. р., ж. р. )
        'двадцатьми' | 'двадцатерых'
    }

    token num:sym<num-30>
    {
        'тридцать'   | 'тридцати' | 'тридцатью' | #`( м. р., с. р., ж. р. )
        'тридцатьми' | 'тридцатерых'
    }

    token num:sym<num-40>
    {
        'сорок'   | 'сорока' | 'сороку'   | 'сороком' | 'сороке' | 'сороки' | 'сороков' | #`( м. р., с. р., ж. р. )
        'сорокам' | 'сороки' | 'сороками' | 'сороках'                                     #`( мн. ч. )
    }

    token num:sym<num-50>
    {
        'пятьдесят'     | 'пятидесяти'     | 'пятьюдесятью' | #`( м. р., с. р., ж. р. )
        'пятидесятерым' | 'пятьмидесятьми' | 'пятидесятерых'  #`( мн. ч. )
    }

    token num:sym<num-60>
    {
        'шестьдесят'     | 'шестидесяти'    | 'шестьюдестью' |  #`( м. р., с. р., ж. р. )
        'шестидесятерых' | 'шестидесятерым' | 'шестидесятерыми' #`( мн. ч. )
    }

    token num:sym<num-70>
    {
        'семьдесят'     | 'семидесяти'    | 'семьюдесятью' | #`( м. р., с. р., ж. р. )
        'семидесятырых' | 'семидесятырым' | 'семидесятырыми'
    }

    token num:sym<num-80>
    {
        'восемьдесят'     | 'восьмидесяти'    | 'восьмьюдесятью' | 'восемьюдесятью' #`( м. р., с. р., ж. р. )
        'восьмидесятырых' | 'восьмидесятырым' | 'восьмидесятырыми'                  #`( мн. ч. )
    }

    token num:sym<num-90>
    {
        'девяносто' | 'девяноста' | 'девяностам' | 'девяностами' | 'девяностах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-100>
    {
        'сто'   | 'ста' | 'стам'  | 'стами'  | 'стах'               #`( м. р., с. р., ж. р. )
        'сотни' | 'сот' | 'сотен' | 'сотням' | 'сотнями' | 'сотнях' #`( мн. ч. )
    }

    token num:sym<num-200>
    {
        'двести' | 'двухсот' | 'двумстам' | 'двумястами' | 'двухстах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-300>
    {
        'триста' | 'трёхсот' | 'трёмстам' | 'тремястами' | 'трёхстах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-400>
    {
        'четыреста' | 'четырёхсот' | 'четырёмстам' | 'четырьмястами' | 'четырёхстах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-500>
    {
        'пятьсот' | 'пятисот' | 'пятистам' | 'пятьюстами' | 'пятистах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-600>
    {
        'шестьсот' | 'шестисот' | 'шестистам' | 'шестьюстами' | 'шестистах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-700>
    {
        'семьсот' | 'семисот' | 'семистам' | 'семьюстами' | 'семистах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-800>
    {
        'восемьсот' | 'восьмисот' | 'восьмистам' | 'восьмьюстами' | 'восемьюстами' | #`( м. р., с. р., ж. р. )
        'восьмистах'                                                                 #`( мн. ч. )
    }

    token num:sym<num-900>
    {
        'девятьсот' | 'девятисот' | 'девятистам' | 'девятьюстами' | 'девятистах' #`( м. р., с. р., ж. р. )
    }

    token num:sym<num-1000>
    {
        'тысяча' | 'тысячи' | 'тысяче'  | 'тысячу'   | 'тысячью' | 'тысячею' | 'тысячей' | #`( м. р., с. р., ж. р. )
        'тысячи' | 'тысяч'  | 'тысячам' | 'тысячами' | 'тысячах'                           #`( мн. ч. )
    }

    token num:sym<num-1000000>
    {
        'миллион'  | 'миллиона'  | 'миллиону'  | 'миллионом'  | 'миллионе'  #`( м. р., с. р., ж. р. )
        'миллионы' | 'миллионов' | 'миллионам' | 'миллионами' | 'миллионах' #`( мн. ч. )
    }

    token num:sym<num-1000000000>
    {
        'миллиард'  | 'миллиарда'  | 'миллиарду'  | 'миллиардом'  | 'миллиарде'  #`( м. р., с. р., ж. р. )
        'миллиарды' | 'миллиардов' | 'миллиардам' | 'миллиардами' | 'миллиардах' #`( мн. ч. )
    }

    # Command part:
    proto token cmd {*}

    #  - h, j, k, l,
    token cmd:sym<move-left>
    {
        'эйч' | 'влево' | 'лево'
    }

    token cmd:sym<move-down>
    {
        'джей' | 'джой' | 'вниз' | 'вниз'
    }

    token cmd:sym<move-up>
    {
        'кей' | 'вверх' | 'верх' | 'наверх'
    }

    token cmd:sym<move-right>
    {
        'эль'    | 'эй'    | 'элли'  | 'алло'  | 'элла' | 'его' |
        'вправо' | 'право' | 'права' | 'павла' | 'пава'
    }

    #  — b,
    token cmd:sym<back> { 'бэ' | 'би' | 'бы' | 'бей' | 'ба' }

    #  — w,
    token cmd:sym<word>
    {
        'вэ' | 'в'
    }

    #  — s,
    token cmd:sym<substitute-char>
    {
        'эс эс' | 'с' | 'эс'
    }

    #  — o,
    token cmd:sym<insert-with-nl>
    {
        'о'
    }

    #  — dd
    token cmd:sym<del>
    {
        <d><space>+<d> | 'диди' | 'деньги'
    }

    #  — u,
    token cmd:sym<undo>
    {
        'у' | 'ю' | 'ему'
    }

    #  — <Ctrl-R>,
    token cmd:sym<redo>
    {
        <ctrl><space>+<r>
    }

    #  — p
    token cmd:sym<paste>
    {
        'пит' | 'пи' | 'по'
    }

    #  - gg,
    token cmd:sym<first-line>
    {
        <g><space>+<g> | 'гага'   | 'егэ'      | 'грэга' | 'бегая'    |
                         'гг'     | 'гы-гы'    | 'губы'  | 'григорию' |
                         'джиджи' | 'джорджии' | 'джорджия'
    }

    #  - G,
    token cmd:sym<last-line>
    {
        <shift><space>+<g>
    }

    #  - i,
    token cmd:sym<insert>
    {
        <i>
    }

    #  - I,
    token cmd:sym<insert-begin>
    {
        <shift><space>+<i>
    }

    #  - a,
    token cmd:sym<insert-next-char>
    {
        <a>
    }

    #  - A,
    token cmd:sym<insert-end>
    {
        <shift><space>+<a>
    }

    #  - ~,
    token cmd:sym<tilde>
    {
        'тильда'  | 'хильда'  | 'сил да' | 'кинга' | 'цель да' | 'тинга'  | 'те да'  |
        'киль да' | 'тень да' | 'тельно' | 'тинга' | 'сильно'  | 'чем да' | 'чей да' |
        'теле да'
    }

    #  - <ESC>,
    token cmd:sym<escape>
    {
        'кип'     | 'скидок' | 'э'     | 'к'      | 'эск'   | 'таск' | 'эскейп' |
        'тайский' | 'скинь'  | 'тоски' | 'скинет' | 'лески' | 'эскей'
    }

    # Modifier keys
    token ctrl
    {
        'контуром' | 'контуру' | 'контрол' | 'контру' | 'комнату' | 'он канал'
    }

    token shift
    {
        'шифт'   | 'шрифт' | 'шутят' | 'шест' | 'шеф' | 'широко' | 'шорт' | 'шафт' |
        'шрифты' | 'шест'  | 'шесть'
    }

    # Latin alphabet
    token a
    {
        'ай' | 'айзек' | 'а' | 'они' | 'азия' | 'айди' | 'аззи' | 'а и' | 'эй'
    }

    token g
    {
        'джи' | 'джим'  | 'дже' | 'джейн' | 'где'  | 'горы' | 'джея' | 'же' |
        'г'   | 'гриль' | 'год' | 'гэри'  | 'грея' | 'глаз'
    }

    token d
    {
        'да' | 'ди'
    }

    token i
    {
        'ай' | 'а ведь' | 'а и' | 'а я'
    }

    token r
    {
        'р'     | 'айро' | 'эрл' | 'эйра' | 'рыбу' | 'эр' | 'амер' | 'энда' | 'анна' |
        'аврал' | 'ара'  | 'эра'
    }

    # Miscellanous:
    #
    #  - <Enter>
    token cmd:sym<enter>
    {
        'энта'  | 'энка'  | 'анка' | 'танта' | 'центр'  | 'энто' | 'лента' | 'антона' |
        'канта' | 'энтер' | 'энто' | 'энд'   | 'антиох' | 'анта' | 'тире да'
    }
}

# Action with Vim command
class VimCmdAction
{
    # Entire command
    method TOP($/)
    {
        make [ $<cmd-with-num>.map(*.made) ];
    }

    method cmd-with-num($/)
    {
        if $<num-full>.made { make $<num-full>.made ~ $<cmd>.made; }
        else                { make $<cmd>.made; }
    }

    # Number part
    method num-full($/)
    {
        my Str $num-full = '0';
        my Str @tmp;

        if $<num>.WHAT ~~ Array
        {
            enum Action <NONE REPLACE ADD CONCAT MULTIPLY>;
            my   Action $action;

            my Str @nums = [ $<num>.map(*.made) ];

            for @nums -> Str $num
            {
                if $num-full eq '0'
                {
                    $action = REPLACE;
                }
                else
                {
                    $action = CONCAT;

                    for (9, 6, 3, 2, 1) -> $len
                    {
                        if $num == '1' ~ '0' x $len
                        {
                            $action = MULTIPLY;
                            last;
                        }

                        if $num-full.chars > $len &&
                           (substr $num-full, *-$len) eq ('0' x $len)
                        {
                            given $num
                            {
                                when '1' le * le ('9' x $len)
                                {
                                    $action = ADD;
                                }

                                default
                                {
                                }
                            }

                            last;
                        }
                    }
                }

                given $action
                {
                    when REPLACE
                    {
                        $num-full = $num;
                    }

                    when ADD
                    {
                        $num-full = ($num-full.Int + $num.Int).Str;
                    }

                    when CONCAT
                    {
                        $num-full ~= $num;
                    }

                    when MULTIPLY
                    {
                        push @tmp, ($num-full.Int * $num.Int).Str;
                        $num-full = '0';
                    }
                }
            }

            push @tmp, $num-full;
            $num-full = shift @tmp;

            for @tmp -> Str $num
            {
                if ($num-full.chars > $num.chars &&
                    substr $num-full, *-$num.chars) eq ('0' x $num.chars)
                {
                    $num-full = ($num-full.Int + $num.Int).Str;
                }
                else
                {
                    $num-full ~= $num;
                }
            }
        }
        else
        {
            $num-full = $<num>.made;
        }

        make $num-full;
    }

    method num:sym<num-0>          ($/) { make          '0'; }
    method num:sym<num-1>          ($/) { make          '1'; }
    method num:sym<num-2>          ($/) { make          '2'; }
    method num:sym<num-3>          ($/) { make          '3'; }
    method num:sym<num-4>          ($/) { make          '4'; }
    method num:sym<num-5>          ($/) { make          '5'; }
    method num:sym<num-6>          ($/) { make          '6'; }
    method num:sym<num-7>          ($/) { make          '7'; }
    method num:sym<num-8>          ($/) { make          '8'; }
    method num:sym<num-9>          ($/) { make          '9'; }
    method num:sym<num-10>         ($/) { make         '10'; }
    method num:sym<num-11>         ($/) { make         '11'; }
    method num:sym<num-12>         ($/) { make         '12'; }
    method num:sym<num-13>         ($/) { make         '13'; }
    method num:sym<num-14>         ($/) { make         '14'; }
    method num:sym<num-15>         ($/) { make         '15'; }
    method num:sym<num-16>         ($/) { make         '16'; }
    method num:sym<num-17>         ($/) { make         '17'; }
    method num:sym<num-18>         ($/) { make         '18'; }
    method num:sym<num-19>         ($/) { make         '19'; }
    method num:sym<num-20>         ($/) { make         '20'; }
    method num:sym<num-30>         ($/) { make         '30'; }
    method num:sym<num-40>         ($/) { make         '40'; }
    method num:sym<num-50>         ($/) { make         '50'; }
    method num:sym<num-60>         ($/) { make         '60'; }
    method num:sym<num-70>         ($/) { make         '70'; }
    method num:sym<num-80>         ($/) { make         '80'; }
    method num:sym<num-90>         ($/) { make         '90'; }
    method num:sym<num-100>        ($/) { make        '100'; }
    method num:sym<num-200>        ($/) { make        '200'; }
    method num:sym<num-300>        ($/) { make        '300'; }
    method num:sym<num-400>        ($/) { make        '400'; }
    method num:sym<num-500>        ($/) { make        '500'; }
    method num:sym<num-600>        ($/) { make        '600'; }
    method num:sym<num-700>        ($/) { make        '700'; }
    method num:sym<num-800>        ($/) { make        '800'; }
    method num:sym<num-900>        ($/) { make        '900'; }
    method num:sym<num-1000>       ($/) { make       '1000'; }
    method num:sym<num-1000000>    ($/) { make    '1000000'; }
    method num:sym<num-1000000000> ($/) { make '1000000000'; }

    # Command part:
    #
    #  - h, j, k, l
    method cmd:sym<move-left> ($/) { make 'h'; }
    method cmd:sym<move-down> ($/) { make 'j'; }
    method cmd:sym<move-up>   ($/) { make 'k'; }
    method cmd:sym<move-right>($/) { make 'l'; }

    #  — b, w, dd, u, <Ctrl-R>
    method cmd:sym<back>            ($/) { make 'b';     }
    method cmd:sym<word>            ($/) { make 'w';     }
    method cmd:sym<substitute-char> ($/) { make 's';     }
    method cmd:sym<insert-with-nl>  ($/) { make 'o';     }
    method cmd:sym<del>             ($/) { make 'dd';    }
    method cmd:sym<undo>            ($/) { make 'u';     }
    method cmd:sym<redo>            ($/) { make '<C-R>'; }
    method cmd:sym<paste>           ($/) { make 'p';     }

    #  - gg, G, i, I, a, A, ~
    method cmd:sym<first-line>       ($/) { make 'gg'; }
    method cmd:sym<last-line>        ($/) { make 'G';  }
    method cmd:sym<insert>           ($/) { make 'i';  }
    method cmd:sym<insert-begin>     ($/) { make 'I';  }
    method cmd:sym<insert-next-char> ($/) { make 'a';  }
    method cmd:sym<insert-end>       ($/) { make 'A';  }
    method cmd:sym<tilde>            ($/) { make '~';  }

    #  - <ESC>
    method cmd:sym<escape> ($/) { make '<ESC>'; }

    # Miscellanous:
    #
    #  - <Enter>
    method cmd:sym<enter> ($/) { make '<Enter>'; }
}
